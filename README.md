# Docker alpine node git ssh

This project aims at build a docker image useful to gitlab CI to build our projects

## USAGE

In gitlab-ci.yml on first line add
```yaml
image: gitlab.com/equileague/tools/docker-alpine-node-git-ssh:latest
```

Then in gitlab-runner you have access to node, git and ssh !

## DEPLOY NEW VERSION

Just git push a tag to gitlab-ci
```bash
git tag [new_tag]
git push origin [new_tag]
```